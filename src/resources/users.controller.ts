import { IncomingMessage, ServerResponse } from 'http';
import {
  UserPartial, Hobbies, UserResponse, HobbiesResponse,
} from '../types/user';
import * as userService from './users.service';
import {
  getRequestBody, extractUserIdFromUrl, formatResponse, joinUserLinks, joinHobbiesLinks,
} from '../utils';
import { ApiError } from '../error-handler';

export const checkIfUserExists = (id: string) => {
  const user = userService.findUser(id);
  if (!user) {
    throw new ApiError(404, `User with id ${id} doesn't exist`);
  }
  return user;
};

export const createUser = async (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
  const reqBody = await getRequestBody<UserPartial>(req);
  const user = userService.createUser(reqBody);
  res.statusCode = 201;
  return formatResponse<UserResponse>(joinUserLinks([user])[0], null);
};

export const getUsers = (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
  const users = userService.getUsers();
  res.statusCode = 200;
  return formatResponse<UserResponse[]>(joinUserLinks(users), null);
};

export const deleteUser = async (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
  const userId = extractUserIdFromUrl(req.url as string) as string;
  checkIfUserExists(userId);

  userService.deleteUser(userId);

  res.statusCode = 200;
  return formatResponse<{ success: boolean}>({ success: true }, null);
};

export const updateHobbies = async (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
  const { hobbies } = await getRequestBody<{ hobbies: Hobbies}>(req);

  const userId = extractUserIdFromUrl(req.url as string) as string;
  checkIfUserExists(userId);

  userService.updateUserHobbies(userId, hobbies);

  res.statusCode = 200;
  return formatResponse<UserResponse>(joinUserLinks([userService.getUserById(userId)])[0], null);
};

export const getHobbies = (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
  const userId = extractUserIdFromUrl(req.url as string) as string;
  checkIfUserExists(userId);

  const hobbies = userService.getUserHobbies(userId);

  res.statusCode = 200;
  return formatResponse<HobbiesResponse>(joinHobbiesLinks(hobbies, userId), null);
};

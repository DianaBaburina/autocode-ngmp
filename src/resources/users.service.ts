import { v4 as uuidv4 } from 'uuid';
import { User, UserPartial } from '../types/user';

let USERS: User[] = [];

export const findUser = (id: string) => USERS.find((u) => u.id === id) as User;

export const createUser = (user: UserPartial) => {
  const newUser: User = { id: uuidv4(), ...user, hobbies: [] };
  USERS.push(newUser);
  return newUser;
};

export const getUserById = (id: string) => findUser(id);

export const getUsers = () => USERS;

export const deleteUser = (id: string) => {
  findUser(id);
  USERS = USERS.filter((u) => u.id !== id);
};

export const updateUserHobbies = (id: string, hobbiesToAdd: string[]) => {
  const userToUpdate = findUser(id);
  Object.assign(userToUpdate, {
    hobbies: [...new Set([...userToUpdate.hobbies, ...hobbiesToAdd])],
  });
};

export const getUserHobbies = (id: string) => {
  const user = findUser(id);
  return user.hobbies;
};

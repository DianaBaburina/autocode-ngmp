import { IncomingMessage } from 'http';
import { Hobbies, User } from './types/user';
import { USERS_API_URL } from './router';

export const getRequestBody = <T> (
  req: IncomingMessage,
): Promise<T> => new Promise((resolve, reject) => {
    let body = '';
    req.on('data', (chunk) => {
      body += chunk.toString();
    });
    req.on('end', () => {
      const data = JSON.parse(body);
      resolve(data);
    });
    req.on('error', (error: Error) => {
      reject(error);
    });
  });

export const extractUserIdFromUrl = (url: string) => {
  const pattern = /^\/api\/users\/(?<uuid>[a-zA-Z0-9_-]+)/;
  const matches = url.match(pattern);
  if (matches?.groups?.uuid) {
    return matches.groups.uuid;
  }
  return null;
};

export const formatResponse = <T>(data: T | null, errorMessage: string | null) => JSON.stringify({
  data: data || null,
  error: errorMessage || null,
});

export const joinUserLinks = (users: User[]) => users.map((user) => {
  const { id, name, email } = user;
  return {
    user: { id, name, email },
    links: {
      self: `${USERS_API_URL}/${id}`,
      hobbies: `${USERS_API_URL}/${id}/hobbies`,
    },
  };
});

export const joinHobbiesLinks = (hobbies: Hobbies, userId: string) => ({
  hobbies,
  links: {
    self: `${USERS_API_URL}/${userId}/hobbies`,
    user: `${USERS_API_URL}/${userId}`,
  },
});

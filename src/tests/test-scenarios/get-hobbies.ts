import request from 'supertest';
import {
  USERS_API_HOST, USERS_API_URL, USERS, HOBBIES,
} from '../constants';
import { createUser, deleteUsers, createUserWithHobbies } from '../users-api';
import { validateHobbies } from '../helpers';

export const getHobbiesTests = () => describe('GET /api/users/{userId}/hobbies', () => {
  const USERS_TO_DELETE: string[] = [];

  test('should return empty list of hobbies if none are added yet', async () => {
    const userId = await createUser(USERS[0]);

    const { body } = await request(USERS_API_HOST)
      .get(`${USERS_API_URL}/${userId}/hobbies`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect('Cache-Control', /private,\s*max-age=3600|max-age=3600,\s*private/)
      .expect(200);

    expect(body).toEqual({ data: validateHobbies(userId, []), error: null });

    USERS_TO_DELETE.push(userId);
  });

  test('should return list of hobbies added for user', async () => {
    const userId = await createUserWithHobbies(USERS[0], HOBBIES);

    const { body } = await request(USERS_API_HOST)
      .get(`${USERS_API_URL}/${userId}/hobbies`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect('Cache-Control', /private,\s*max-age=3600|max-age=3600,\s*private/)
      .expect(200);

    expect(body).toEqual({ data: validateHobbies(userId, HOBBIES), error: null });

    USERS_TO_DELETE.push(userId);
  });

  test('should return error if user doesn\'t exist', async () => {
    const randomUserId = 'a07bfb92-1458-4f0d-a385-b866e30f6ca0';

    const { body, headers } = await request(USERS_API_HOST)
      .get(`${USERS_API_URL}/${randomUserId}/hobbies`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404);

    expect(body).toEqual({
      data: null,
      error: `User with id ${randomUserId} doesn't exist`,
    });
    expect(headers).not.toHaveProperty('Cache-Control');
  });

  afterAll(async () => {
    await deleteUsers(USERS_TO_DELETE);
  });
});

import request from 'supertest';
import { USERS_API_HOST, USERS_API_URL, USERS } from '../constants';
import { createUser, deleteUsers, getUsers } from '../users-api';

export const deleteUserTests = () => describe('DELETE /api/users', () => {
  const USERS_TO_DELETE: string[] = [];

  test('should delete user', async () => {
    const userId = await createUser(USERS[0]);

    const { body } = await request(USERS_API_HOST)
      .delete(`${USERS_API_URL}/${userId}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body).toEqual({
      data: { success: true },
      error: null,
    });

    const users = await getUsers();
    expect(users.find((u) => u.id === userId)).toEqual(undefined);

    USERS_TO_DELETE.push(userId);
  });

  test('should return error if user doesn\'t exist', async () => {
    const randomUserId = 'a07bfb92-1458-4f0d-a385-b866e30f6ca0';

    const { body } = await request(USERS_API_HOST)
      .delete(`${USERS_API_URL}/${randomUserId}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404);

    expect(body).toEqual({
      data: null,
      error: `User with id ${randomUserId} doesn't exist`,
    });
  });

  afterAll(async () => {
    await deleteUsers(USERS_TO_DELETE);
  });
});

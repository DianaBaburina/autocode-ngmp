import request from 'supertest';
import { USERS_API_HOST, USERS_API_URL, USERS } from '../constants';
import { createUser, deleteUsers, deleteUsersAll } from '../users-api';
import { validateUser } from '../helpers';

export const getUsersTests = () => describe('GET /api/users', () => {
  const USERS_TO_DELETE: string[] = [];

  test('should return empty list of users if none are created yet', async () => {
    await deleteUsersAll();

    const { body } = await request(USERS_API_HOST)
      .get(USERS_API_URL)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect('Cache-Control', /public,\s*max-age=3600|max-age=3600,\s*public/)
      .expect(200);

    expect(body).toEqual({ data: [], error: null });
  });

  test('should return list of users created', async () => {
    const userId1 = await createUser(USERS[0]);
    const userId2 = await createUser(USERS[1]);

    const { body } = await request(USERS_API_HOST)
      .get(USERS_API_URL)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect('Cache-Control', /public,\s*max-age=3600|max-age=3600,\s*public/)
      .expect(200);

    expect(body).toEqual({
      data: [
        validateUser({ id: userId1, ...USERS[0] }),
        validateUser({ id: userId2, ...USERS[1] }),
      ],
      error: null,
    });

    USERS_TO_DELETE.push(userId1, userId2);
  });

  afterAll(async () => {
    await deleteUsers(USERS_TO_DELETE);
  });
});

import request from 'supertest';
import { USERS_API_HOST, USERS_API_URL, USERS } from '../constants';
import { validateUser } from '../helpers';
import { deleteUsers, getUsers } from '../users-api';

export const createUserTests = () => describe('POST /api/users', () => {
  const USERS_TO_DELETE: string[] = [];

  test('should create new user', async () => {
    const { body } = await request(USERS_API_HOST)
      .post(USERS_API_URL)
      .send(USERS[0])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201);

    const { id: userId } = body.data.user;

    expect(body).toEqual({ data: validateUser({ id: userId, ...USERS[0] }), error: null });

    const users = await getUsers();
    expect(users.find((u) => u.id === userId)).toEqual({ id: userId, ...USERS[0] });

    USERS_TO_DELETE.push(userId);
  });

  afterAll(async () => {
    await deleteUsers(USERS_TO_DELETE);
  });
});

import request from 'supertest';
import {
  USERS_API_HOST, USERS_API_URL, USERS, HOBBIES,
} from '../constants';
import {
  createUser, deleteUsers, getHobbies, createUserWithHobbies,
} from '../users-api';
import { validateUser } from '../helpers';

export const updateHobbiesTests = () => describe('PATCH /api/users/{userId}/hobbies', () => {
  const USERS_TO_DELETE: string[] = [];

  test('should update user hobbies - add new if there are none', async () => {
    const userId = await createUser(USERS[0]);

    const { body } = await request(USERS_API_HOST)
      .patch(`${USERS_API_URL}/${userId}/hobbies`)
      .send({ hobbies: HOBBIES })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body).toEqual({ data: validateUser({ id: userId, ...USERS[0] }), error: null });

    const hobbies = await getHobbies(userId);
    expect(hobbies).toEqual(HOBBIES);

    USERS_TO_DELETE.push(userId);
  });

  test('should update user hobbies - add new to existing hobbies', async () => {
    const userId = await createUserWithHobbies(USERS[0], HOBBIES);
    const newHobbies = ['traveling'];

    const { body } = await request(USERS_API_HOST)
      .patch(`${USERS_API_URL}/${userId}/hobbies`)
      .send({ hobbies: newHobbies })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body).toEqual({ data: validateUser({ id: userId, ...USERS[0] }), error: null });

    const hobbies = await getHobbies(userId);
    expect(hobbies).toEqual([...HOBBIES, ...newHobbies]);

    USERS_TO_DELETE.push(userId);
  });

  test('should update user hobbies - add new to existing hobbies, duplicates are removed', async () => {
    const userId = await createUserWithHobbies(USERS[0], HOBBIES);
    const newHobbies = ['traveling', 'programming']; // programming is duplicate

    const { body } = await request(USERS_API_HOST)
      .patch(`${USERS_API_URL}/${userId}/hobbies`)
      .send({ hobbies: newHobbies })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body).toEqual({ data: validateUser({ id: userId, ...USERS[0] }), error: null });

    const hobbies = await getHobbies(userId);
    expect(hobbies).toEqual([...HOBBIES, 'traveling']);

    USERS_TO_DELETE.push(userId);
  });

  test('should return error if user doesn\'t exist', async () => {
    const randomUserId = 'a07bfb92-1458-4f0d-a385-b866e30f6ca0';

    const { body } = await request(USERS_API_HOST)
      .patch(`${USERS_API_URL}/${randomUserId}/hobbies`)
      .send({ hobbies: [] })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404);

    expect(body).toEqual({
      data: null,
      error: `User with id ${randomUserId} doesn't exist`,
    });
  });

  afterAll(async () => {
    await deleteUsers(USERS_TO_DELETE);
  });
});

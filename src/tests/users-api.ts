/* eslint-disable max-len */
import request from 'supertest';
import { USERS_API_HOST, USERS_API_URL } from './constants';
import { UserPartial, User } from './types';

export const createUser = async (user: UserPartial): Promise<string> => {
  const { body: { data: { user: { id } } } } = await request(USERS_API_HOST)
    .post(USERS_API_URL)
    .send(user)
    .set('Accept', 'application/json');

  return id;
};

export const updateHobbies = async (userId: string, hobbies: string[]): Promise<void> => {
  await request(USERS_API_HOST)
    .patch(`${USERS_API_URL}/${userId}/hobbies`)
    .send({ hobbies })
    .set('Accept', 'application/json');
};

export const createUserWithHobbies = async (user: UserPartial, hobbies: string[]): Promise<string> => {
  const userId = await createUser(user);
  await updateHobbies(userId, hobbies);
  return userId;
};

export const deleteUsers = async (userIdsCreated: string[]): Promise<void> => {
  await Promise.all(userIdsCreated.map((id) => request(USERS_API_HOST).delete(`${USERS_API_URL}/${id}`)));
};

export const getUsers = async (): Promise<User[]> => {
  const { body } = await request(USERS_API_HOST).get(USERS_API_URL).set('Accept', 'application/json');
  return body.data.map((u: { user: User }) => u.user);
};

export const deleteUsersAll = async () => {
  const users = await getUsers();
  if (users.length) {
    await deleteUsers(users.map((u) => u.id));
  }
};

export const getHobbies = async (userId: string): Promise<string[]> => {
  const { body } = await request(USERS_API_HOST).get(`${USERS_API_URL}/${userId}/hobbies`);
  return body.data.hobbies;
};

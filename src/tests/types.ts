export type UserPartial = { name: string; email: string }

export type User = { id: string } & UserPartial;

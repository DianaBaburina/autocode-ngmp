import { USERS_API_URL } from './constants';
import { User } from './types';

export const validateUser = (user: User) => ({
  user: {
    id: expect.any(String),
    name: user.name,
    email: user.email,
  },
  links: {
    self: `${USERS_API_URL}/${user.id}`,
    hobbies: `${USERS_API_URL}/${user.id}/hobbies`,
  },
});

export const validateHobbies = (userId: string, hobbies: string[]) => ({
  hobbies,
  links: {
    self: `${USERS_API_URL}/${userId}/hobbies`,
    user: `${USERS_API_URL}/${userId}`,
  },
});

import { createUserTests } from './test-scenarios/create-user';
import { deleteUserTests } from './test-scenarios/delete-user';
import { getHobbiesTests } from './test-scenarios/get-hobbies';
import { getUsersTests } from './test-scenarios/get-users';
import { updateHobbiesTests } from './test-scenarios/update-hobbies';

describe('Module 4 - Network', () => {
  createUserTests();
  deleteUserTests();
  getHobbiesTests();
  getUsersTests();
  updateHobbiesTests();
});

export const USERS_API_HOST = 'http://localhost:8000';
export const USERS_API_URL = '/api/users';

export const USERS = [
  { name: 'john', email: 'john@john.john' },
  { name: 'ann', email: 'ann@ann.ann' },
];

export const HOBBIES = ['dancing', 'sport', 'programming'];

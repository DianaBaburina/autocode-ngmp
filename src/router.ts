import { IncomingMessage, ServerResponse } from 'http';
import { Route, HTTPMethod } from './types/router';
import * as userController from './resources/users.controller';

export const USERS_API_URL = '/api/users';

export const createRouter = (
  req: IncomingMessage,
  res: ServerResponse<IncomingMessage>,
): Route[] => [
  {
    // GET /api/users
    regex: /^\/api\/users$/,
    method: HTTPMethod.GET,
    handler: () => userController.getUsers(req, res),
    cache: {
      ttl: 3600,
      private: false,
    },
  },
  {
    // POST /api/users
    regex: /^\/api\/users$/,
    method: HTTPMethod.POST,
    handler: () => userController.createUser(req, res),
  },
  {
    // DELETE /api/users
    regex: /^\/api\/users\/(?<uuid>[a-zA-Z0-9_-]+)$/,
    method: HTTPMethod.DELETE,
    handler: () => userController.deleteUser(req, res),
  },
  {
    // PATCH /api/users/:userId/hobbies
    regex: /^\/api\/users\/(?<uuid>[a-zA-Z0-9_-]+)\/hobbies$/,
    method: HTTPMethod.PATCH,
    handler: () => userController.updateHobbies(req, res),
  },
  {
    // GET /api/users/:userId/hobbies
    regex: /^\/api\/users\/(?<uuid>[a-zA-Z0-9_-]+)\/hobbies$/,
    method: HTTPMethod.GET,
    handler: () => userController.getHobbies(req, res),
    cache: {
      ttl: 3600,
      private: true,
    },
  },
];

export const findRoute = (
  router: Route[],
  url: string,
  method: string,
) => router.find((r) => r.method === method && new RegExp(r.regex).test(url));

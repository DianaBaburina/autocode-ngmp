import http from 'http';
import { ApiError, handleErrors } from './error-handler';
import { createRouter, findRoute } from './router';

const PORT = process.env.PORT || 8000;

const server = http.createServer(async (req, res) => {
  try {
    if (!req.url || !req.method) {
      throw new ApiError(404, 'Not found');
    }

    const router = createRouter(req, res);
    const route = findRoute(router, req.url, req.method);
    if (!route) {
      throw new ApiError(404, 'Not found');
    }

    const data = await route.handler();
    if (route.cache) {
      res.setHeader('Cache-Control', `${route.cache.private ? 'private' : 'public'}, max-age=${route.cache.ttl}`);
    }
    res.setHeader('Content-Type', 'application/json');
    res.end(data);
  } catch (error) {
    handleErrors(error as ApiError, req, res);
  }
});

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

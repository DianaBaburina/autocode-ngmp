export enum HTTPMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}

export type RouteHandler = () => void;

export type Route = {
  regex: RegExp,
  method: HTTPMethod,
  handler: RouteHandler,
  cache?: {
    ttl: number;
    private: boolean;
  },
};

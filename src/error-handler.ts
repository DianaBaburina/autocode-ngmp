import http from 'http';
import { formatResponse } from './utils';

export class ApiError extends Error {
  constructor(
    public readonly statusCode: number,
    public readonly message: string,
  ) {
    super(`Status code - ${statusCode}, description - ${message}`);
  }
}

export const handleErrors = (
  error: ApiError,
  req: http.IncomingMessage,
  res: http.ServerResponse<http.IncomingMessage>,
) => {
  const statusCode = error.statusCode || 500;
  const errorMessage = error.message || 'Internal server error';

  res.setHeader('Content-Type', 'application/json');
  res.statusCode = statusCode;
  res.end(formatResponse(null, errorMessage));
};
